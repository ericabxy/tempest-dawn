# Tempest Dawn

A new RPG developed for a retro-style browser game engine.

Tempest Dawn is a fork of Heroine Dusk by Clint Bellanger.

## License

* All of the visual art and code for Heroine Dusk was created by Clint Bellanger. http://clintbellanger.net
* The code for Heroine Dusk is released under GPL v3, with later versions permitted.
* The visual art for Heroine Dusk is released under CC-BY-SA 3.0, with later versions permitted.
* The music is by Yubatake (CC-BY 3.0). http://opengameart.org/users/yubatake

## Attribution

The copyright licenses above allow you to share Heroine Dusk. When making it available on other platforms, be sure to include the proper attribution.

Here is a suggested way to attribute Heroine Dusk (to fulfill the terms of Creative Commons attribution)

    Heroine Dusk is created by Clint Bellanger http://clintbellanger.net
    Heroine Dusk features music by Yubatake http://opengameart.org/users/yubatake
